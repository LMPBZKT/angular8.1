import { Component, OnInit } from '@angular/core';
import { DatosUsuario } from 'src/app/interfaces/tarjeta.interface';
import {MatTableDataSource, MatTableModule} from '@angular/material/table';
import { TarjetaService } from 'src/app/servicios/tarjeta.service';
import { MatSnackBar } from '@angular/material/snack-bar';



@Component({
  selector: 'app-listado-tarjeta',
  templateUrl: './listado-tarjeta.component.html',
  styleUrls: ['./listado-tarjeta.component.css']
})


export class ListadoTarjetaComponent implements OnInit {

  listaDeUsuarios:DatosUsuario[] = [];
  displayedColumns: string[] = ['titulo', 'numero', 'expiracion', 'accion'];
  dataSource !: MatTableDataSource<any>;


  constructor(private _usuarioService:TarjetaService, private _snackbar:MatSnackBar) { }

  ngOnInit(): void {
    this.cargar();
  }
  cargar(){
  this.listaDeUsuarios=this._usuarioService.getUsuario();
  this.dataSource=new MatTableDataSource(this.listaDeUsuarios);
}
eliminarUsuario(index:number){
console.log(index);
this._usuarioService.eliminar(index);
this.cargar();
this._snackbar.open('Usuario eliminado','',{
  duration:1500,
  horizontalPosition:'center',
  verticalPosition:'bottom'
})
}
}
