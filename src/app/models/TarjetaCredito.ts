//41.
export class TarjetaCredito {
  id?:string;
  titulo:string
  numero:string;
  expiracion:string;
  contrasena:number;
  fechaCreacion:Date;
  fechaActualizacion:Date;

  constructor (titulo:string,numero:string,expiracion:string,contrasena:number){
    this.titulo=titulo;
    this.numero=numero;
    this.expiracion=expiracion;
    this.contrasena=contrasena;
    this.fechaCreacion= new Date ();
    this.fechaActualizacion = new Date();
  }
}