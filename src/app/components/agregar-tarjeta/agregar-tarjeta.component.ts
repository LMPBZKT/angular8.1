import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TarjetaCredito } from 'src/app/models/TarjetaCredito';

@Component({
  selector: 'app-agregar-tarjeta',
  templateUrl: './agregar-tarjeta.component.html',
  styleUrls: ['./agregar-tarjeta.component.css']
})
export class AgregarTarjetaComponent implements OnInit {

  form: FormGroup;
  constructor(private fb: FormBuilder) { 
    this.form = this.fb.group({
      titulo:['',Validators.required],
      numero: ['',[Validators.required, Validators.minLength(16),Validators.maxLength(16)]],
      expiracion: ['',[Validators.required, Validators.minLength(5),Validators.maxLength(5)]],
      contrasena:['', [Validators.required, Validators.minLength(3),Validators.maxLength(3)]]
    })
  }

crear(){
  const tarjeta:TarjetaCredito ={
    titulo:this.form.value.titulo,
    numero:this.form.value.numero,
    expiracion:this.form.value.expiracion,
    contrasena:this.form.value.contrasena,
    fechaCreacion: new Date(),
    fechaActualizacion:new Date()
  };
  console.log(tarjeta);
  
}
    
  

  ngOnInit(): void {
  }

}
