export interface TarjetaCredito {

  titulo:string
  numero:string;
  expiracion:string;
  contrasena:number;
  fechaCreacion:Date;
  fechaActualizacion:Date;
}