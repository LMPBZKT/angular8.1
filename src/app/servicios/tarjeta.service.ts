import { Injectable } from '@angular/core';
import { DatosUsuario } from '../interfaces/tarjeta.interface';



@Injectable({
  providedIn:'root'
})



export class TarjetaService {
   listaDeUsuarios: DatosUsuario[] = [
    {titulo:'Adrian', numero: '123456789123456789', expiracion: '31/22'},
    {titulo:'Juan', numero: '123456789100456789', expiracion: '14/23'},
    {titulo:'Ivan', numero: '123456789123456789', expiracion: '24/25'},
    {titulo:'Jhosep', numero: '123456789123456789', expiracion: '50/26'},
    {titulo:'Miguel', numero: '123456789123456789', expiracion: '12/22'},
  ];
  constructor() { }

  getUsuario(){
    return this.listaDeUsuarios.slice()
  }

  eliminar(index:number){
    this.listaDeUsuarios.splice(index,1);
  }
}
